configfile: "config.yaml"

from snakemake.utils import makedirs
from snakemake.utils import listfiles

import os
import sys
import numpy as np
import pandas as pd
import yaml

def generate_windows(wildcards):
    #SLIDING WINDOWS
    windows = []
    s = -1* int(config["region_size_up"])
    e = s + int(config["window_size"])
    while e <= int(config["region_size_down"]):
        windows.append((s,e))
        s = s + int(config["window_step"])
        e = e + int(config["window_step"])
    #convert to dirextory names
    windows_regions = []
    for win in windows:
        if win[0]<0 and win[1]<=0: #upstream
            windows_regions.append("u"+str(-1*win[0])+"to"+str(-1*win[1])+".d0to0")
        elif win[0]>=0 and win[1]>0: #downstream
            windows_regions.append("u0to0.d"+str(win[0])+"to"+str(win[1]))
        else: #window goes through 0
            windows_regions.append("u"+str(-1*win[0])+"to0.d0to"+str(win[1]))
    return windows_regions

# read in the table with the samples
samples = pd.read_table(config['design.table'], index_col=0)

localrules: all, create_log_dirs, tandem_pas_repSites_to_bed, download_genome, prepare_bam_indices, merge_TIN_vals, TIN_assessment, plot_full_trans_TIN_distributions, tpm_normalize, get_filtered_expression, rel_pos_of_pAs, get_filtered_rel_usages, weighted_average_exon_length, plot_average_exon_length, create_matrix_dir, create_polyAsite2exon_mapping, prepare_expression_matrix, merge_single_regions_all, select_top_kmers_and_scores

################################################################################
# define the target rule
################################################################################

rule all:
    ## LOCAL ##
    '''
    Defines the target rule by specifying all final output files.
    '''
    input:
        outfile_standarized = lambda wildcards: \
                              expand(os.path.join("{output_dir}",
                                                  "site_count_matrices",
                                                  "{reg}",
                                                  "matrix.tsv"),
                                     output_dir = config["dir.results"],
                                     reg = generate_windows(wildcards)),
        TIN_distributions = os.path.join(config['dir.results'],
                                         "bias.TIN.all_transcripts.boxplots.pdf"),
        exon_length_CDF_plot = os.path.join(config['dir.results'],
                                            "weighted_avg_exonLength.CDFs.pdf"),
        TSV_analysis_top_kmers = os.path.join(config["dir.results"],
                                              ("plots_for_top"
                                               + str(config['nr_selected_kmers'])
                                               + "_kmers"),
                                              "mean_diff_zscores.tsv")

################################################################################
# rules in chronological order
################################################################################

#-------------------------------------------------------------------------------
# create directories
#-------------------------------------------------------------------------------
rule create_log_dirs:
    ##LOCAL##
    output:
        created_dirs=touch(os.path.join(config["dir.logs"], "created_dirs.log"))
    params:
        cluster_logs = os.path.join(config["dir.logs"],"cluster_logs")
    shell:
        """
        mkdir -p {params.cluster_logs}
        """

#-------------------------------------------------------------------------------
# download genome sequences
#-------------------------------------------------------------------------------
rule download_genome:
    ##LOCAL##
    output:
        FASTA_genome = config["genome.fasta"]
    params:
        URL = config['genome.url']
    conda:
        os.path.join(config["dir.conda"], "wget_gzip.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        wget -O genome_tmp.fa.gz \
        {params.URL} \
        -q --show-progress \
        2>> {log.local_err}
        gzip -d genome_tmp.fa.gz && \
        mv genome_tmp.fa {output.FASTA_genome}
        '''

#-------------------------------------------------------------------------------
# create bam indices
#-------------------------------------------------------------------------------
rule prepare_bam_indices:
    ##LOCAL##
    input:
        bam = lambda wildcards: os.path.join(config['dir.input'], samples.loc[ wildcards.sample, "bam"])
    output:
        bam_bai = touch(os.path.join(config['dir.logs'], "{sample}", "created_bai.log"))
    params:
        bam_bai = lambda wildcards: os.path.join(config['dir.input'], samples.loc[ wildcards.sample, "bam"] + ".bai")
    conda:
        os.path.join(config["dir.conda"], "samtools.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        samtools index {input.bam} {params.bam_bai} \
        2>> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# assess coverage bias transcript wide (via TIN measure)
#-------------------------------------------------------------------------------
rule assess_TIN_bias_transcript_wide:
    input:
        created_dirs=touch(os.path.join(config["dir.logs"], "created_dirs.log")),
        created_bam_index = os.path.join(config['dir.logs'], "{sample}", "created_bai.log"),
        bam = lambda wildcards: os.path.join(config['dir.input'], samples.loc[wildcards.sample, "bam"]),
    output:
        tin_vals = os.path.join(config['dir.results'], "samplewise_TIN", "{sample}.tsv")
    params:
        script_dir = config['dir.scripts'],
        transcripts = config['transcripts'],
        min_raw_reads = config['qc.rawReads.min'],
        sample_size = config['qc.sampleSize'],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "{sample}.log")
    log:
        os.path.join(config["dir.logs"], "{sample}", "assess_TIN_bias_transcript_wide.log")
    resources:
        time = 24
    conda:
        os.path.join(config["dir.conda"], "rseqc.yaml")
    shell:
        '''
        python {params.script_dir}/calculate-TIN-values.py \
        -i {input.bam} \
        -r {params.transcripts} \
        -c {params.min_raw_reads} \
        --names {wildcards.sample} \
        -n {params.sample_size} \
        > {output.tin_vals} \
        2> {log}
        '''

#-------------------------------------------------------------------------------
# merge single sample TIN-vals tables
#-------------------------------------------------------------------------------
rule merge_TIN_vals:
    ##LOCAL##
    input:
        tables = expand( os.path.join(config['dir.results'],
                                      "samplewise_TIN",
                                      "{sa}.tsv"), sa = samples.index )
    output:
        tin_table = os.path.join(config['dir.results'], "bias.TIN.all_transcripts.tsv")
    params:
        script_dir = config['dir.scripts']
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    conda:
        os.path.join(config["dir.conda"], "python2.yaml")
    shell:
        '''
        python {params.script_dir}/merge-TIN-tables.py \
        --verbose \
        --input {input.tables} \
        > {output.tin_table} \
        2>> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# create a table with the medianTIN values per sample
#-------------------------------------------------------------------------------
rule TIN_assessment:
    ## LOCAL ##
    input:
        table = os.path.join(config['dir.results'], "bias.TIN.all_transcripts.tsv")
    output:
        overview = os.path.join(config['dir.results'],
                                "bias.TIN.all_transcripts.median_per_sample.tsv")
    run:
        transcript_vals = {}
        names = []
        medians = []
        with open(input.table, "r") as f:
            for line in f:
                F = line.rstrip().split("\t")
                if line.startswith("transcript"):
                    # header line; store names
                    for idx in range(1, len(F)):
                        transcript_vals[ idx - 1 ] = []
                        names.append( F[idx] )
                else:
                    for idx in range(1, len(F)):
                        val = float( F[idx])
                        if val != 0.0:
                            transcript_vals[ idx -1 ].append( val )

        for idx in sorted(transcript_vals):
            medians.append( np.median( transcript_vals[idx]) )

        with open(output.overview, "w") as out:
            out.write("sample\tmedian_TIN\n")
            for idx in range(len(names)):
                curr_name = names[idx]
                curr_median = medians[idx]
                out.write("%s\t%f\n" % (curr_name, curr_median))

#-------------------------------------------------------------------------------
# plot the boxplots of full-transcript TIN vals per sample
#-------------------------------------------------------------------------------
rule plot_full_trans_TIN_distributions:
    ##LOCAL##
    input:
        med_TIN = os.path.join(config['dir.results'], "bias.TIN.all_transcripts.tsv")
    output:
        plot = os.path.join(config['dir.results'],
                            "bias.TIN.all_transcripts.boxplots.pdf")
    params:
        script_dir = config['dir.scripts']
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        Rscript {params.script_dir}/boxplots-TIN-distributions.R \
	--file {input} \
	--pdf {output} \
        2>> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# get the terminal exon read coverage  profiles
#-------------------------------------------------------------------------------
rule create_coverages:
    input:
        created_dirs=touch(os.path.join(config["dir.logs"], "created_dirs.log")),
        bam = lambda wildcards: os.path.join(config['dir.input'],
                                             samples.loc[wildcards.sample, "bam"]),
        created_bam_index = os.path.join(config['dir.logs'], "{sample}", "created_bai.log")
    output:
        pkl_object = os.path.join(config['dir.results'], "coverages", "{sample}.pkl")
    params:
        script_dir = config['dir.scripts'],
        clusters= config['tandem_pas'],
        ds_extend=config['cvg.ds_extend'],
        start2prox_minDist=config['cvg.start2prox_minDist'],
        unstranded = lambda wildcards: "--unstranded" if config['cvg.unstranded'] == "yes" else "",
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "{sample}.log")
    resources:
        mem = lambda wildcards: int( np.ceil(50 / config['cvg.threads'] ) ),
        time = 6
    threads:
        config['cvg.threads']
    log:
        os.path.join(config["dir.logs"], "{sample}", "create_coverages.log")
    conda:
        os.path.join(config["dir.conda"], "htseq_python2.yaml")
    shell:
        '''
        python {params.script_dir}/mRNAseq-coverage-objects-and-wiggleFiles.py \
        --verbose \
        --bam {input.bam} \
        --cluster {params.clusters} \
        --ds_extension {params.ds_extend} \
        --min_dist_exStart2prox {params.start2prox_minDist} \
        --processors {threads} \
        --pickle_out {output.pkl_object} {params.unstranded} \
        2> {log}
        '''

#-------------------------------------------------------------------------------
# infer used poly(A) sites based on the coverage profiles
# and determine for those the relative usage
#-------------------------------------------------------------------------------
rule infer_relative_usage:
    input:
        coverages = expand(os.path.join(config['dir.results'],
                                        "coverages",
                                        "{sample}.pkl"), sample= samples.index)
    output:
        relUse = os.path.join(config['dir.results'], "relative_usages.tsv"),
	expressions = os.path.join(config['dir.results'], "tandem_pas_expressions.tsv"),
        distal_sites = os.path.join(config['dir.results'], "single_distal_sites.tsv")
    params:
        script_dir = config['dir.scripts'],
        clusters = config['tandem_pas'],
        conditions = lambda wildcards: list(samples.loc[:, "group"]),
        us_extensions = expand(os.path.join(config['dir.results'],
                                            "coverages",
                                            "{sample}.extensions.tsv"), sample= samples.index),
        names = lambda wildcards: list(samples.index),
        read_len = config['readLength'],
        min_region_length_to_infer_meanCov = config['relUse.minLength.meanCvg'],
        rawCounts_minMeanCoverage_per_sample = config['relUse.minMeanCvg.perSample'],
        downstream2search4noCov = config['relUse.distal_ds'],
        max_downstream_cov_relative_to_start = config['relUse.distal_ds.maxCvg'],
        min_cluster_distance = config['relUse.min_cluster_distance'],
        valid_region_upstream_of_cluster_to_locate_globalBestBreakPoint_in = config['relUse.us_reg_for_best_breakPoint'],
        mse_ratio_threshold = config['relUse.mse_ratio_threshold'],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "infer_relative_usage.log")
    threads: config['relUse.threads']
    resources:
        mem = lambda wildcards: int( np.ceil(32 / config['relUse.threads'] ) ),
        time = 6
    log:
        os.path.join(config["dir.logs"],"infer_relative_usage.log")
    conda:
        os.path.join(config["dir.conda"], "htseq_python2.yaml")
    shell:
        '''
        python {params.script_dir}/deNovo-used-sites-and-usage-inference.single_distal_included.py \
        --verbose \
	--expressions_out {output.expressions} \
        --clusters {params.clusters} \
        --coverages {input.coverages} \
        --conditions {params.conditions} \
	--ex_extension_files {params.us_extensions} \
        --names {params.names} \
        --read_length {params.read_len} \
        --min_coverage_region {params.min_region_length_to_infer_meanCov} \
        --min_mean_coverage {params.rawCounts_minMeanCoverage_per_sample} \
        --ds_reg_for_no_coverage {params.downstream2search4noCov} \
        --min_cluster_distance {params.min_cluster_distance} \
        --mse_ratio_threshold {params.mse_ratio_threshold} \
        --best_break_point_upstream_extension {params.valid_region_upstream_of_cluster_to_locate_globalBestBreakPoint_in} \
        --processors {threads} \
        --max_downstream_coverage {params.max_downstream_cov_relative_to_start} \
        --distal_sites {output.distal_sites} \
        > {output.relUse} \
        2> {log}
        '''

#-------------------------------------------------------------------------------
# tpm normalize the expression values by the number of mapped reads
#-------------------------------------------------------------------------------
rule tpm_normalize:
    ##LOCAL##
    input:
        expressions = os.path.join(config['dir.results'], "tandem_pas_expressions.tsv"),
        distal_sites = os.path.join(config['dir.results'], "single_distal_sites.tsv")
    output:
        tpm_expr = os.path.join(config['dir.results'], "tandem_pas_expressions.rpm.tsv")
    run:
        libsizes = []
        with open(input.expressions, "r") as ifile:
            for line in ifile:
                if line.startswith("chrom"):
                    continue
                F = line.rstrip().split("\t")
                if len(libsizes) == 0:
                    for i in range(10, len(F)):
                        libsizes.append(0)
                for i in range(10, len(F)):
                    if float(F[i]) == -1:
                        continue
                    libsizes[ i - 10 ] += float(F[i])

        with open(input.distal_sites, "r") as distal_in:
            for line in distal_in:
                if line.startswith("chrom"):
                    continue
                for i in range(10, len(F)):
                    if float(F[i]) == -1:
                        continue
                    libsizes[ i - 10 ] += float(F[i])

        with open(output.tpm_expr, "w") as ofile:
            with open(input.expressions, "r") as ifile:
                for line in ifile:
                    if line.startswith("chrom"):
                        ofile.write("%s" % line)
                        continue
                    F = line.rstrip().split("\t")
                    for i in range(10, len(F)):
                        if float(F[i]) == -1:
                            continue
                        F[i] = "{0:.6f}".format( float(F[i]) / libsizes[ i - 10 ] * 1000000 )
                    ofile.write("%s\n" % "\t".join(F))

#-------------------------------------------------------------------------------
# filter expression table: remove all exons for which any site was
# not expressed in all samples
#-------------------------------------------------------------------------------
rule get_filtered_expression:
    ##LOCAL##
    input:
        tpm_expr = os.path.join(config['dir.results'], "tandem_pas_expressions.rpm.tsv")
    output:
        tpm_expr_filtered = os.path.join(config['dir.results'],
                                         "tandem_pas_expressions.rpm.filtered.tsv")
    run:
        exons = {}
        header = ''
        with open(input.tpm_expr, "r") as expr_in:
            for line in expr_in:
                if line.startswith("chrom"):
                    header = line
                    continue
                selected_entries = []
                F = line.rstrip().split("\t")
                if F[8] not in exons:
                    exons[ F[8] ] = []
                selected_entries += F[0:10]
                consider = True
                for idx in range(10, len(F)):
                    curr_val = float(F[idx])
                    selected_entries.append( curr_val )
                    if curr_val == -1.0:
                        consider = False
                        break
                if consider:
                    exons[ F[8] ].append(selected_entries)
                else:
                    exons[ F[8] ].append( None )

        with open(output.tpm_expr_filtered, "w") as out_table_expr:
            out_table_expr.write("%s" % header)
            for ex in exons:
                if None in exons[ex]:
                    continue
                for site_list in exons[ex]:
                    out_table_expr.write("%s\n" % "\t".join([str(i) for i in site_list]))

#-------------------------------------------------------------------------------
# convert the representative sites of the tandem clusters into bed format
#-------------------------------------------------------------------------------
rule tandem_pas_repSites_to_bed:
    ##LOCAL##
    input:
        tandem_pas = os.path.join(config['dir.results'],
                                  "tandem_pas_expressions.rpm.filtered.tsv")
    output:
        BED_repSites = os.path.join( config['dir.results'],
                                     "tandem_pas_expressions.rpm.filtered.repSites.bed")
    run:
        with open(input.tandem_pas, "r") as ifile, open(output.BED_repSites, "w") as ofile:
            for line in ifile:
                if line.startswith("chrom"):
                    continue
                F = line.rstrip().split("\t")
                S = F[3].split(":")
                ofile.write("%s\t%i\t%i\t%s\t%s\t%s\n" %
                            (F[0],
                             (int(S[2]) - 1),
                             int(S[2]),
                             F[3],
                             F[4],
                             F[5]))

#-------------------------------------------------------------------------------
# get the relative position of poly(A) sites within the exon
#-------------------------------------------------------------------------------
rule rel_pos_of_pAs:
    ##LOCAL##
    input:
        os.path.join(config['dir.results'], "relative_usages.tsv")
    output:
        os.path.join(config['dir.results'], "relative_usages.relPos_per_pA.out")
    params:
        script_dir = config['dir.scripts']
    conda:
        os.path.join(config["dir.conda"], "python2.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        python {params.script_dir}/relative-pas-position-within-exon.py \
        --relUsage {input} \
        > {output} \
        2>> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# create a version of the tables with tandem PAS expressions that does only
# contain exons for which all poly(A) sites were quantified in all samples
#-------------------------------------------------------------------------------
rule get_filtered_rel_usages:
    ##LOCAL##
    input:
        rel_use = os.path.join(config['dir.results'],
                               "relative_usages.tsv")
    output:
        rel_use_filtered = os.path.join(config['dir.results'],
                                        "relative_usages.filtered.tsv")
    run:
        exons = {}
        with open(input.rel_use, "r") as ifile:
            for line in ifile:
                F = line.rstrip().split("\t")
                if F[8] not in exons:
                    exons[ F[8] ] = []
                selected_entries = F[0:10]
                consider = True
                for idx in range(10,len(F)):
                    curr_val = float(F[idx])
                    selected_entries.append( curr_val )
                    if curr_val == -1.0:
                        consider = False
                        break
                if consider:
                    exons[ F[8] ].append(selected_entries)
                else:
                    exons[ F[8] ].append( None )

            with open(output.rel_use_filtered, "w") as out_table:
                for ex in exons:
                    if None in exons[ex]:
                        continue
                    for site_list in exons[ex]:
                        out_table.write("%s\n" % "\t".join([str(i) for i in site_list]))

#-------------------------------------------------------------------------------
# get weighted average exon length
#-------------------------------------------------------------------------------
rule weighted_average_exon_length:
    ##LOCAL##
    input:
        relUse = os.path.join(config['dir.results'],
                              "relative_usages.filtered.tsv"),
        relPos = os.path.join(config['dir.results'],
                              "relative_usages.relPos_per_pA.out")
    output:
        os.path.join(config['dir.results'], "weighted_avg_exonLength.tsv")
    params:
        script_dir = config['dir.scripts'],
        names = lambda wildcards: " ".join( list(samples.index))
    conda:
        os.path.join(config["dir.conda"], "python2.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        python {params.script_dir}/calculate-average-3pUTR-length.py \
        --samples {params.names} \
        --relativePos {input.relPos} \
        --relUsage {input.relUse} \
        > {output} \
        2>> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# plot the cumulative distribution for the weighted average exon lengths
# per sample
#-------------------------------------------------------------------------------
rule plot_average_exon_length:
    ##LOCAL##
    input:
        exon_length = os.path.join(config['dir.results'],
                                   "weighted_avg_exonLength.tsv")
    output:
        exon_length_cdf_plot = os.path.join(config['dir.results'],
                                            "weighted_avg_exonLength.CDFs.pdf")
    params:
        script_dir = config['dir.scripts'],
        study = config['study.name']
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        Rscript {params.script_dir}/plot-ecdfs.R \
        --main={params.study}_samples \
        --pdf={output.exon_length_cdf_plot} \
        --file={input.exon_length} \
        2>> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# create directory for site count matrices
#-------------------------------------------------------------------------------
rule create_matrix_dir:
    ##LOCAL##
    output:
        matrices_dir = os.path.join(config["dir.results"],
                               "site_count_matrices")
    log:
        local_err = os.path.join(config["dir.logs"],
                                 "local_stderr.log")
    shell:
        """
        mkdir -p {output.matrices_dir}
        """

#-------------------------------------------------------------------------------
# Create windows dirs, extract window regions in bed, sequences in fasta
#-------------------------------------------------------------------------------
rule extract_sequences:
    input:
        created_dirs=touch(os.path.join(config["dir.logs"], "created_dirs.log")),
        matrices_dir = os.path.join(config["dir.results"],
                               "site_count_matrices"),
        BED_repSites = os.path.join( config['dir.results'],
                                     "tandem_pas_expressions.rpm.filtered.repSites.bed"),
        genome = config["genome.fasta"]
    output:
        seq_fasta = os.path.join(config["dir.results"],
                                 "site_count_matrices",
                                 "{reg}",
                                 "sequences.fasta")
    params:
        script_dir = config['dir.scripts'],
        outdir = os.path.join(config["dir.results"],
                              "site_count_matrices",
                              "{reg}"),
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "{reg}.log")
    resources:
        mem = 10
    log:
        local_err = os.path.join(config["dir.results"],
                                 "site_count_matrices",
                                 "local_stderr.log")
    conda:
        os.path.join(config["dir.conda"], "pybedtools.yaml")
    shell:
        """
        python {params.script_dir}/mb-extract-sequences.py \
        --bed {input.BED_repSites} \
        --genome {input.genome} \
        --outdir {params.outdir} \
        2>> {log.local_err}
        """

#-------------------------------------------------------------------------------
# Create sitecount matrices
#-------------------------------------------------------------------------------
rule create_kmer_sitecount_matrix:
    input:
        seq_fasta = os.path.join(config["dir.results"],
                                 "site_count_matrices",
                                 "{reg}",
                                 "sequences.fasta")
    output:
        outfile = os.path.join(config["dir.results"],
                               "site_count_matrices",
                               "{reg}",
                               "matrix.tsv")
    params:
        script_dir = config['dir.scripts'],
        kmer_min = config["kmer_min"],
        kmer_max = config["kmer_max"],
        outdir = os.path.join(config["dir.results"],
                              "site_count_matrices",
                              "{reg}"),
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "{reg}.log")
    resources:
        mem = 10
    log:
        local_err = os.path.join(config["dir.results"],
                                 "site_count_matrices",
                                 "local_stderr.log")
    conda:
        os.path.join(config["dir.conda"], "py3_numpy.yaml")
    shell:
        """
        python {params.script_dir}/mb-sitecount-matrix-kmers.py \
        --window-dir {params.outdir} \
        --kmer-min {params.kmer_min} \
        --kmer-max {params.kmer_max} \
        2>> {log.local_err}
        """

#-------------------------------------------------------------------------------
# create polyAsite to gene mappings
#-------------------------------------------------------------------------------
rule create_polyAsite2exon_mapping:
    ##LOCAL##
    input:
        tandem_pas = os.path.join(config['dir.results'],
                                  "tandem_pas_expressions.rpm.filtered.tsv")
    output:
        TSV_polyAsite2exon = os.path.join(config["dir.resources"], "polyAsite2exon.tsv")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        """
        cut -f 4,9 {input.tandem_pas} \
        1> {output.TSV_polyAsite2exon} \
        2>> {log.local_err}
        """    
        
#-------------------------------------------------------------------------------
# Tag poly(A) sites as overlapping/not overlapping
#-------------------------------------------------------------------------------
rule tag_overlapping_polyAsites:
    input:
        created_dirs_log = os.path.join(config["dir.logs"], "created_dirs.log"),
        TSV_polyAsite_expression = os.path.join(config['dir.results'],
                                                "tandem_pas_expressions.rpm.filtered.tsv")
    output:
        TSV_tagged_polyAsite_expression = \
                                     os.path.join(config["dir.results"],
                                                  "tandem_pas_expression_tagged.tsv")
    params:
        script_dir = config['dir.scripts'],
        INT_region_size_up = config["region_size_up"],
        INT_region_size_down = config["region_size_down"],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "tag_overlapping_polyAsites.log")
    log:
        LOG_file = \
                   os.path.join(config["dir.logs"],
                                "tag_overlapping_polyAsites.log")
    conda:
        os.path.join(config["dir.conda"],
                     "perl.yaml")
    shell:
        """
        perl {params.script_dir}/filter-tandem-PAS-by-length.pl \
          --upstream={params.INT_region_size_up} \
          --downstream={params.INT_region_size_down} \
          {input.TSV_polyAsite_expression} \
        1> {output.TSV_tagged_polyAsite_expression} \
        2> {log.LOG_file}
        """

#-------------------------------------------------------------------------------
# prepare expression matrix for first run
#-------------------------------------------------------------------------------
rule prepare_expression_matrix:
    ##LOCAL##
    input:
        TSV_tagged_polyAsite_expression = \
                                     os.path.join(config["dir.results"],
                                                  "tandem_pas_expression_tagged.tsv")
    output:
        TSV_pas_expression = \
                                     os.path.join(config["dir.results"],
                                                  "tandem_pas_expression_final.tsv")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        cut --complement -f 1-3,5-10 {input.TSV_tagged_polyAsite_expression} \
        > {output.TSV_pas_expression} \
        2> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# fit model for current region
#-------------------------------------------------------------------------------
rule fit_single_region_all:
    input:
        TSV_sc_matrix = os.path.join(config["dir.results"],
                                     "site_count_matrices",
                                     "{window}",
                                     "matrix.tsv"),
        TSV_design_file = config["design.table"],
        TSV_pas_expression = os.path.join(config["dir.results"],
                                          "tandem_pas_expression_final.tsv"),
        TSV_pas2exon = os.path.join(config["dir.resources"], "polyAsite2exon.tsv")
    output:
        TSV_meanDiffZscores = os.path.join(config["dir.results"],
                                           "{window, u\d+.*to\d+}",
                                           "mean_diff_zscores.tsv")
    wildcard_constraints:
        #window="^u\d+.*to\d\d$"
    params:
        script_dir = config['dir.scripts'],
        results_dir = os.path.join(config["dir.results"], "{window}"),
        row_center_expr = config["row_center_expr"],
        treat_minus_ctrl = config["treat_minus_ctrl"],
        tpm_pseudocount = config["tpm_pseudocount"],
        only_excess_counts = config["only_excess_counts"],
        sliding_window_size = config["window_size"],
        min_kmer_abundance = config["min_kmer_abundance"],
        nr_random_runs = config["nr_random_runs"],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "fit_{window}_region_all.log")
    resources:
        mem = 32
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    log:
        LOG_file = \
                   os.path.join(config["dir.logs"],
                                "fit_{window}_region_all.log")
    shell:
        '''
        Rscript --vanilla {params.script_dir}/KAPAC.R \
        --verbose TRUE \
        --sample_design {input.TSV_design_file} \
        --expression_matrix {input.TSV_pas_expression} \
        --sitecount_matrix {input.TSV_sc_matrix} \
        --pas_exon_associations {input.TSV_pas2exon} \
        --row_center_expression {params.row_center_expr} \
        --treat_minus_ctrl {params.treat_minus_ctrl} \
        --expression_pseudocount {params.tpm_pseudocount} \
        --consider_excess_counts_only {params.only_excess_counts} \
        --considered_region_length {params.sliding_window_size} \
        --min_kmer_abundance_fraction {params.min_kmer_abundance} \
        --number_of_randomized_runs {params.nr_random_runs} \
        --results_dir {params.results_dir} \
        &> {log.LOG_file}
        '''

#-------------------------------------------------------------------------------
# merge results
#-------------------------------------------------------------------------------
rule merge_single_regions_all:
    ##LOCAL##
    input:
        TSV_meanDiffZscores = lambda wildcards: \
                              expand( os.path.join(config["dir.results"],
                                                   "{window}",
                                                   "mean_diff_zscores.tsv"),
                                      window = generate_windows(wildcards))
    output:
        TSV_meanDiffZscores_all = os.path.join(config["dir.results"],
                                               "mean_diff_zscores_ALL.tsv")
    params:
        windows = generate_windows,
        nr_of_kmers_to_consider = config["overall_nr_results_to_consider"],
        # change column to 0-based format
        column_for_sorting = config["sorting_all_results_by_col"] - 1,
        added_column_name = "region"
    run:
        first_file = True
        kmer_entries = []
        with open(output.TSV_meanDiffZscores_all, "w") as out_f:
            for window in params.windows:
                current_file = os.path.join(config["dir.results"],
                                            window,
                                            "mean_diff_zscores.tsv")
                with open(current_file, "r") as in_f:
                    header = next(in_f)
                    if first_file:
                        # only print the header once
                        first_file = False
                        out_f.write("%s\t%s\n" % (header.rstrip(), params.added_column_name))
                    line_cnt = 0
                    for line in in_f:
                        line_cnt += 1
                        if line_cnt > params.nr_of_kmers_to_consider:
                            break
                        
                        F = line.rstrip().split("\t")
                        F.append(window)
                        kmer_entries.append(F)

            # get the top n kmers according to the requested column
            # print the top params.nr_of_kmers_to_consider
            top_kmers = sorted(kmer_entries,
                               key = lambda entry: \
                               abs(float(entry[ params.column_for_sorting ])))[-params.nr_of_kmers_to_consider:]
            for entry in top_kmers[::-1]:
                out_f.write("%s\n" % "\t".join(entry))

#-------------------------------------------------------------------------------
# select the top kmers for plotting
#-------------------------------------------------------------------------------
rule select_top_kmers:
    input:
        TSV_meanDiffZscores_all = os.path.join(config["dir.results"],
                                               "mean_diff_zscores_ALL.tsv")
    output:
        TSV_meanDiffZscores_selected = os.path.join(config["dir.results"],
                                                    "mean_diff_zscores_SELECTED.tsv")
    params:
        script_dir = config['dir.scripts'],
        nr_selected_kmers = config['nr_selected_kmers'],
        selection_criterion = config["select_col_name"],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "select_top_kmers.log")
    log:
        LOG_file = \
                   os.path.join(config["dir.logs"],
                                "select_top_kmers.log")
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    shell:
        '''
        Rscript --vanilla {params.script_dir}/collapse_top_kmers_from_different_regions.R \
        --top_kmers_list {input.TSV_meanDiffZscores_all} \
        --nr_top_kmers_to_plot {params.nr_selected_kmers} \
        --top_kmer_candidates_selection_criterion {params.selection_criterion} \
        --out_file {output.TSV_meanDiffZscores_selected} \
        &> {log.LOG_file}
        '''

#-------------------------------------------------------------------------------
# re-format the table with selected kmers
# keep another table with only the kmers and scores
#-------------------------------------------------------------------------------
rule select_top_kmers_and_scores:
    ##LOCAL##
    input:
       TSV_meanDiffZscores_selected = os.path.join(config["dir.results"],
                                                    "mean_diff_zscores_SELECTED.tsv")
    output:
        TSV_meanDiffZscores_selected_kmers = os.path.join(config["dir.results"],
                                                          "mean_diff_zscores_SELECTED.kmers.tsv")
    params:
        # another first column was added in the step before
        column_for_selecting = config["sorting_all_results_by_col"] + 1
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    log:
        local_err = os.path.join(config["dir.logs"], "local_stderr.log")
    shell:
        '''
        cut -f2 {input.TSV_meanDiffZscores_selected} \
        > {output.TSV_meanDiffZscores_selected_kmers} \
        2> {log.local_err}
        '''

#-------------------------------------------------------------------------------
# second run of KAPAC with selected kmers
#-------------------------------------------------------------------------------
rule fit_single_region_selected:
    input:
        TSV_meanDiffZscores_selected_kmers = os.path.join(config["dir.results"],
                                                          "mean_diff_zscores_SELECTED.kmers.tsv"),
        TSV_sc_matrix = os.path.join(config["dir.results"],
                                     "site_count_matrices",
                                     "{window}",
                                     "matrix.tsv"),
        TSV_design_file = config["design.table"],
        TSV_pas_expression = os.path.join(config["dir.results"],
                                          "tandem_pas_expression_final.tsv"),
        TSV_pas2exon = os.path.join(config["dir.resources"], "polyAsite2exon.tsv")
    output:
        TSV_meanDiffZscores = os.path.join(config["dir.results"],
                                           "{window, u\d+.*to\d+}",
                                           "SELECTED",
                                           "mean_diff_zscores.tsv")
    params:
        script_dir = config['dir.scripts'],
        results_dir = os.path.join(config["dir.results"],
                                   "{window}",
                                   "SELECTED"),
        row_center_expr = config["row_center_expr"],
        treat_minus_ctrl = config["treat_minus_ctrl"],
        tpm_pseudocount = config["tpm_pseudocount"],
        only_excess_counts = config["only_excess_counts"],
        sliding_window_size = config["window_size"],
        min_kmer_abundance = 0,
        nr_random_runs = config["nr_random_runs"],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "fit_{window}_region_selected.log")
    resources:
        mem = 32
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    log:
        LOG_file = \
                   os.path.join(config["dir.logs"],
                                "fit_{window}_region_selected.log")
    shell:
        '''
        Rscript --vanilla {params.script_dir}/KAPAC.R \
        --verbose TRUE \
        --sample_design {input.TSV_design_file} \
        --expression_matrix {input.TSV_pas_expression} \
        --sitecount_matrix {input.TSV_sc_matrix} \
        --pas_exon_associations {input.TSV_pas2exon} \
        --selected_motifs {input.TSV_meanDiffZscores_selected_kmers} \
        --create_plots_for_each_motif TRUE \
        --create_files_for_each_motif TRUE \
        --row_center_expression {params.row_center_expr} \
        --treat_minus_ctrl {params.treat_minus_ctrl} \
        --expression_pseudocount {params.tpm_pseudocount} \
        --consider_excess_counts_only {params.only_excess_counts} \
        --considered_region_length {params.sliding_window_size} \
        --min_kmer_abundance_fraction {params.min_kmer_abundance} \
        --number_of_randomized_runs {params.nr_random_runs} \
        --results_dir {params.results_dir} \
        &> {log.LOG_file}
        '''

#-------------------------------------------------------------------------------
# plot the activity profiles for the top kmers
#-------------------------------------------------------------------------------
rule plot_activity_profiles:
    input:
        TSV_meanDiffZscores_secondRun = lambda wildcards: \
                                        expand( os.path.join(config["dir.results"],
                                                             "{window}",
                                                             "SELECTED",
                                                             "mean_diff_zscores.tsv"),
                                                window = generate_windows(wildcards)),
        TSV_meanDiffZscores_selected = os.path.join(config["dir.results"],
                                                    "mean_diff_zscores_SELECTED.tsv"),
        TSV_design_file = config["design.table"]
    output:
        TSV_analysis_top_kmers = os.path.join(config["dir.results"],
                                              ("plots_for_top"
                                               + str(config['nr_selected_kmers'])
                                               + "_kmers"),
                                              "mean_diff_zscores.tsv")
    params:
        script_dir = config['dir.scripts'],
        results_dir = os.path.join(config["dir.results"],
                                   ("plots_for_top"
                                    + str(config['nr_selected_kmers'])
                                    + "_kmers"),
                                   "activity_profiles"),
        regions = lambda wildcards: ":".join(generate_windows(wildcards)),
        nr_selected_kmers = config['nr_selected_kmers'],
        regions_dir = config['dir.results'],
        treat_minus_ctrl = config["treat_minus_ctrl"],
        cluster_log = os.path.join(config["dir.logs"],
                                   "cluster_logs",
                                   "plot_activity_profiles.log")
    resources:
        mem = 32
    conda:
        os.path.join(config["dir.conda"], "r.yaml")
    log:
        LOG_file = \
                   os.path.join(config["dir.logs"],
                                "plot_activity_profiles.log")
    shell:
        '''
        Rscript --vanilla {params.script_dir}/plot_kmers_of_all_regions.R \
        --sample_design {input.TSV_design_file} \
        --kmers_to_plot_matrix {input.TSV_meanDiffZscores_selected} \
        --nr_top_kmers_to_plot {params.nr_selected_kmers} \
        --regions_dir {params.regions_dir} \
        --regions {params.regions} \
        --results_dir {params.results_dir} \
        --treat_minus_ctrl {params.treat_minus_ctrl} \
        2> {log.LOG_file}
        '''
