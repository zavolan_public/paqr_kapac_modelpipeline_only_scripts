# clean the workspace
rm(list=ls())

# load libraries
##library(data.table)
library(optparse)
library(gplots)
library('RColorBrewer')

# _____________________________________________________________________________
# -----------------------------------------------------------------------------
# get the parameters
# -----------------------------------------------------------------------------
option_list <- list(
  make_option(c("--sample_design"), action="store", type="character", help="The sample design file."),
  make_option(c("--kmers_to_plot_matrix"), action="store", type="character", help="The kmer matrix sorted from top (most significant) to bottom."),
  make_option(c("--nr_top_kmers_to_plot"), action="store", type="numeric", help="The number of kmers for which a plot should be created."),  
  make_option(c("--regions_dir"), action="store", type="character", help="The directory in which the region directories can be found."),
  make_option(c("--regions"), action="store", type="character", help="colon separated list of regions"),
  make_option(c("--results_dir"), action="store", type="character", help="The directory for the result files."),
  make_option(c("--treat_minus_ctrl"), action="store", type="logical", help="If true, plot treatment minus control."))
opt_parser <- OptionParser(usage="Usage: %prog [OPTIONS]",
                           option_list = option_list, add_help_option=TRUE)
opt <- parse_args(opt_parser)

# _____________________________________________________________________________
# -----------------------------------------------------------------------------
# for debugging
##debugging_mode = TRUE
debugging_mode = FALSE
if (debugging_mode == TRUE)
{
  opt = list()
  opt$sample_design = "INPUT_TMP/kapac_design.tsv"
  opt$kmers_to_plot_matrix = "RESULTS_TMP/mean_diff_zscores_SELECTED.tsv"
  opt$nr_top_kmers_to_plot = 20
  opt$regions_dir = "RESULTS_TMP"
  opt$regions = "u0to0.d0to50"
  opt$results_dir = "RESULTS_TMP/plots_for_top20_kmers/activity_profiles"
  opt$treat_minus_ctrl = FALSE
}

# get the input parameters permanently
sample_design = opt$sample_design
kmers_to_plot_matrix = opt$kmers_to_plot_matrix
nr_top_kmers_to_plot = opt$nr_top_kmers_to_plot
regions_dir = opt$regions_dir
regions_str = opt$regions
results_dir = opt$results_dir
treat_minus_ctrl = opt$treat_minus_ctrl

# _____________________________________________________________________________
# -----------------------------------------------------------------------------
# define the functions we need
# -----------------------------------------------------------------------------

write_incl_rownames <- function(data, col_name, filename)
{
  data.incl_rownames =
    cbind(rownames(data),
          data)
  colnames(data.incl_rownames)[1] = col_name
  write.table(data.incl_rownames, file=filename,
              quote=F, row.names=F, col.names=T, sep="\t")
  cat(paste('Wrote successfully: ', filename, '\n', sep=''))
}

create_design_table <- function(sample_design_file_path)
{
  design_table = 
    read.table(sample_design_file_path, 
               h=TRUE, 
               as.is=T, 
               sep="\t",
               comment.char="",
               check.names=FALSE)
  design_table$name = apply(design_table, 1, function(x) {paste(x['group'], x['sample_id'], sep='_')} )
  rownames(design_table) = design_table$name
  return(design_table)
}

calculateActivityDifference <- function(activity_ctrl,
                                        activity_treat)
{
  activity_difference = activity_treat - activity_ctrl
  return(activity_difference)
}

calculateDeltaDifference <- function(delta_ctrl,
                                     delta_treat)
{ 
  delta_difference = sqrt(delta_treat^2.0 + delta_ctrl^2.0)
  return(delta_difference)
} 

calcultateZValue <- function(activity_difference,
                             delta_difference)
{ 
  zvalue = activity_difference / delta_difference
  return(zvalue)
}

# -----------------------------------------------------------------------------
# specify the results directory
##results_dir='kmer_plots'
dir.create(results_dir, showWarnings = FALSE)

# _____________________________________________________________________________
# -----------------------------------------------------------------------------
# Read in the table with the kmers
# -----------------------------------------------------------------------------
kmers.to_plot = 
  read.table(kmers_to_plot_matrix, 
             h=TRUE, 
             as.is=T, 
             row.names=1,
             sep="\t",
             comment.char="",
             check.names=FALSE)

# remove some k-mers in case we don't want to plot that many k-mers
kmers.to_plot = kmers.to_plot[1:(min(nrow(kmers.to_plot),nr_top_kmers_to_plot)),]

# _____________________________________________________________________________
# -----------------------------------------------------------------------------
# Read in the design table
# -----------------------------------------------------------------------------
# read in the design matrix
design_table = create_design_table(sample_design)

# -----------------------------------------------------------------------------
# function to collect the activity change
get_activities_and_deltas <- function(regions_dir,
                                      design_table,
                                      kmer,
                                      region_pattern)
{
  # ---------------------------------------------------------------------------
  # get the regions
  # regions = as.data.frame(Sys.glob(paste(regions_dir, "u*.d*", sep="/")))
  regions = as.data.frame(strsplit(regions_str, ":")[[1]], stringsAsFactors= F)
  colnames(regions) = c('name')
  regions$path = unlist(lapply(regions$name, function(x){ file.path(regions_dir, x, fsep = .Platform$file.sep)}))

  # create a matrix that contains the region informations
  get_max_region_value <- function(x)
  {
    max(as.numeric(unlist(strsplit(x=substr(x,start=2,stop=nchar(x)),split='[to]'))),na.rm=TRUE)
  }
  ##get_max_region_value(x="u0to0")
  max_region_matrix = 
    apply(matrix(unlist(strsplit(x=regions$name, split='[.]')), ncol=2, byrow=T), 
          c(1,2), get_max_region_value)
  colnames(max_region_matrix) = c('upstream','downstream')
  # put minus values to the upstream values
  max_region_matrix[,'upstream'] = max_region_matrix[,'upstream'] * -1
  # create one column that can be used for sorting the regions later on
  regions$pos =
    apply(max_region_matrix, 1,
          function(x) { 
          if ((x['upstream'] != 0) & (x['downstream'] != 0)) {
            return(min(x))
          } else if (x['downstream'] == 0) {
            return(min(x))
          } else if (x['upstream'] == 0) {
            return(max(x))
          } else {
            return(NULL)
          }} )
  # sort the regions
  regions = regions[order(regions$pos,decreasing=F),]
  rownames(regions) = regions$name
  
  # ---------------------------------------------------------------------------
  # get the activities and deltas
  if ('CNTRL' %in% design_table$contrast)
  {
    # _________________________________________________________________________
    # -------------------------------------------------------------------------
    # Create an activities difference plot
    # -------------------------------------------------------------------------
    # create pairs
    contrast_pairs = list()
    for (TREAT_name in design_table[(design_table$contrast != 'CNTRL'),'name']) 
    {
      if (design_table[TREAT_name,'contrast'] %in% design_table$sample_id) 
      {
	CNTRL_id = design_table[TREAT_name,'contrast']
	CNTRL_name = paste( design_table[design_table$sample_id == CNTRL_id,'name'])
	TREAT_id = design_table[TREAT_name,'sample_id']
        contrast_pairs[[paste(TREAT_name, 'vs', CNTRL_name, sep="_")]] = c(TREAT_id, CNTRL_id)
      } else {
        warning(paste("Control sample", 
                      design_table[TREAT_name,'contrast'], "specified as contrast for sample", 
                      design_table[TREAT_name,'sample_id'], "could not be found in the design table. Sample skipped from analysis!", sep=" "))
      }
    }
    
    # -------------------------------------------------------------------------
    # determine if we should plot treatment minus control or
    # control minus treatment
    if (treat_minus_ctrl == TRUE) {
      treat_idx = 1
      ctrl_idx = 2
      ylabel = 'activity difference (treat - ctrl)'
    } else if (treat_minus_ctrl == FALSE) {
      treat_idx = 2
      ctrl_idx = 1
      ylabel = 'activity difference (ctrl - treat)'
    } else {
      stop(paste('treat_minus_ctrl must be TRUE or FALSE!', sep=""))
    }
    
    # -------------------------------------------------------------------------
    # create matrices for the activities and the deltas
    activities.mat = matrix(nrow=length(names(contrast_pairs)),ncol=nrow(regions),
                            dimnames=list(names(contrast_pairs), regions$pos))
    deltas.mat = matrix(nrow=length(names(contrast_pairs)),ncol=nrow(regions),
                        dimnames=list(names(contrast_pairs), regions$pos))
    zscores.mat = matrix(nrow=length(names(contrast_pairs)),ncol=nrow(regions),
                         dimnames=list(names(contrast_pairs), regions$pos))
    fov.mat = matrix(nrow=length(names(contrast_pairs)),ncol=nrow(regions),
                     dimnames=list(names(contrast_pairs), regions$pos))
    
    # fill the matrices
    ##region_pos=-100
    for (region_pos in regions$pos)
    {  
      # get the region definition
      region_name = regions[(regions$pos == region_pos),'name']
      ##cat(paste(region_name, '\n', sep=''))
      if (length(region_name) != 1) 
      {
        stop(paste('There were found ', length(region_name), ' regions associated with region position ', region_pos, '. Please redefine the regions so that every position up- or downstream occures only once!', sep=""))
      }
      
      # get the path to the file
      results_path =
        file.path(as.character(regions[region_name,'path']), 
              "SELECTED", fsep=.Platform$file.sep)
      
      # -----------------------------------------------------------------------------
      # read in the activities
      activities = 
        read.csv(paste(results_path, kmer,'activities.tsv', sep='/'), 
                 h=T, row.names=1, check.names=F, as.is=T, sep="\t")
      deltas = 
        read.csv(paste(results_path, kmer,'deltas.tsv', sep='/'), 
                 h=T, row.names=1, check.names=F, as.is=T, sep="\t")
      # fov = 
      #   read.csv(paste(results_path, kmer,'fov.tsv', sep='/'), 
      #            h=T, row.names=1, check.names=F, as.is=T, sep="\t")
      
      # calculate the activity differences for each contrast
      ##contrast = names(contrast_pairs)[1]
      for (contrast in names(contrast_pairs)) 
      {
        ##contrast = names(contrast_pairs)[1]
        # calculate the activity differences
        activities.mat[contrast,as.character(region_pos)] = 
          calculateActivityDifference(activity_ctrl=activities[contrast_pairs[[contrast]][ctrl_idx],kmer],
                                      activity_treat=activities[contrast_pairs[[contrast]][treat_idx],kmer])
        deltas.mat[contrast,as.character(region_pos)] = 
          calculateDeltaDifference(delta_ctrl=deltas[contrast_pairs[[contrast]][ctrl_idx],kmer],
                                   delta_treat=deltas[contrast_pairs[[contrast]][treat_idx],kmer])
        zscores.mat[contrast,as.character(region_pos)] = 
          calcultateZValue(activity_difference=activities.mat[contrast,as.character(region_pos)],
                           delta_difference=deltas.mat[contrast,as.character(region_pos)])
        # fov.mat[contrast,as.character(region_pos)] = 
        #   calculateExplainedFov(delta_ctrl=fov[contrast_pairs[[contrast]][ctrl_idx],kmer],
        #                         delta_treat=fov[contrast_pairs[[contrast]][treat_idx],kmer])
      }    
    }
    
    # return the values
    # return(list("activities"=activities.mat,
    #             "deltas"=deltas.mat,
    #             "regions"=regions,
    #             "ylabel"=ylabel,
    #             "zscore"=zscores.mat,
    #             "fov"=fov.mat))
    return(list("activities"=activities.mat,
                "deltas"=deltas.mat,
                "regions"=regions,
                "ylabel"=ylabel,
                "zscore"=zscores.mat))
  } else {
    # _________________________________________________________________________
    # -------------------------------------------------------------------------
    # Create absolute activities plot
    # -------------------------------------------------------------------------
    warning(paste("No control samples (CNTRL) found in the contrast column of the design table, thus activitues will be plotted without considering any contrast!", sep=""))
    
    # set the ylabel
    ylabel = 'activities'
    
    # create matrices for the activities and the deltas
    activities.mat = matrix(nrow=nrow(design_table),ncol=nrow(regions),
                            dimnames=list(design_table$name, regions$pos))
    deltas.mat = matrix(nrow=nrow(design_table),ncol=nrow(regions),
                        dimnames=list(design_table$name, regions$pos))
    fov.mat = matrix(nrow=nrow(design_table),ncol=nrow(regions),
                        dimnames=list(design_table$name, regions$pos))
    
    # get the activities and deltas for the current regions
    ##region_pos=regions$pos[1]
    for (region_pos in regions$pos)
    {  
      # get the region definition
      region_name = regions[(regions$pos == region_pos),'name']
      
      # get the region name
      if (length(region_name) != 1) 
      {
        stop(paste('There were found ', length(region_name), ' regions associated with region position ', region_pos, '. Please redefine the regions so that every position up- or downstream occures only once!', sep=""))
      }
      
      # get the path to the file
      results_path =
        paste(as.character(regions[region_name,'path']), "/RESULTS_",
              centering, "_selected_kmers_polyAmara_run_for_region_", 
              region_name, sep='')
                  
      # -----------------------------------------------------------------------------
      # read in the activities and add them to the matrix
      activities = 
        read.csv(paste(results_path, kmer,'activities.tab', sep='/'), 
                 h=T, row.names=1, check.names=F, as.is=T, sep="\t")
      activities.mat[rownames(activities),as.character(region_pos)] = activities[,kmer]
      # read in the deltas and add them to the matrix
      deltas = 
        read.csv(paste(results_path, kmer,'deltas.tab', sep='/'), 
                 h=T, row.names=1, check.names=F, as.is=T, sep="\t")
      deltas.mat[rownames(deltas),as.character(region_pos)] = deltas[,kmer]
      # read in the fraction of explained variance
      fov = 
        read.csv(paste(results_path, kmer,'fov.tab', sep='/'), 
                 h=T, row.names=1, check.names=F, as.is=T, sep="\t")
      fov.mat[rownames(deltas),as.character(region_pos)] = fov[,kmer]
    }
    
    # return the values
    return(list("activities"=activities.mat,
                "deltas"=deltas.mat,
                "regions"=regions,
                "ylabel"=ylabel,
                "fov"=fov.mat))
  }
}

# _____________________________________________________________________________
# -----------------------------------------------------------------------------
# Create one plot per k-mer
# -----------------------------------------------------------------------------
##kmer_nr = rownames(kmers.to_plot)[1]
plot_type = "pdf"
##plot_height = 7
##plot_width = 14

selected_rows <- c()

# create one plot for each k-mer
##kmer_nr = rownames(kmers.to_plot)[1]
for (kmer_nr in rownames(kmers.to_plot))
{  
  # get the k-mer data
  kmer = kmers.to_plot[kmer_nr,'kmer']
    
  # get the upstream results
  activities_and_deltas = 
    get_activities_and_deltas(regions_dir=regions_dir,
                              design_table=design_table,
                              kmer=kmer)
  
  # ---------------------------------------------------------------------------
  # create the plot
  file_name = paste(kmer_nr, kmer, sep="_")

  # set the position as the rownames in the regions  
  rownames(activities_and_deltas$regions) = activities_and_deltas$regions$pos

  # calculate the upper and the lower bounds
  upper_bound = activities_and_deltas$activities + 1.96*activities_and_deltas$deltas
  lower_bound = activities_and_deltas$activities - 1.96*activities_and_deltas$deltas

  # for z-scores
  zscores.upper_bound = activities_and_deltas$zscore
  zscores.lower_bound = activities_and_deltas$zscore
  
  # # for fov
  # fov.upper_bound = activities_and_deltas$fov
  # fov.lower_bound = activities_and_deltas$fov
  
  # create the layout
  if (nrow(activities_and_deltas$activities) <= 4) {
    # create the pdf
    pdf(file.path(results_dir, paste(file_name, '.pdf', sep=''), fsep=.Platform$file.sep),
        height=6, width=14, useDingbats=F)
    # create the layout
    layout(matrix(c(1,2), nrow=1, ncol=2, byrow = TRUE))
    # get some nice colors (maximal 12 can be chosen)
    colors = brewer.pal(12, 'Paired')[1:nrow(activities_and_deltas$activities)]
  } else if ((nrow(activities_and_deltas$activities) > 4) 
             & (nrow(activities_and_deltas$activities) <= 12)) {
    # create the pdf
    pdf(file.path(results_dir, paste(file_name, '.pdf', sep=''), fsep=.Platform$file.sep), 
        height=6, width=21, useDingbats=F)
    # create the layout
    layout(matrix(c(1,1,2), nrow=1, ncol=3, byrow = TRUE))
    # get some nice colors (maximal 12 can be chosen)
    colors = brewer.pal(12, 'Paired')[1:nrow(activities_and_deltas$activities)]
  } else if (nrow(activities_and_deltas$activities) > 12) {
    # create the pdf
    pdf(file.path(results_dir, paste(file_name, '.pdf', sep=''), fsep=.Platform$file.sep),
        height=6, width=28, useDingbats=F)
    # create the layout
    layout(matrix(c(1,1,1,2), nrow=1, ncol=4, byrow = TRUE))
    # HINT: Twelfe is the limit for the color brewer palet!!
    colors = colorRampPalette(c("blue4", "yellow", "red4") )(nrow(activities_and_deltas$activities))
  }
    
  # create the plot
  par(mar=c(10,8,2,2), mgp=c(4,1,0), las=2)
  barplot2(activities_and_deltas$activities,
           names.arg = activities_and_deltas$regions[colnames(activities_and_deltas$activities),'name'],
           beside = TRUE,
           plot.ci = TRUE,                                  # logical. If TRUE, confidence intervals are plotted over the bars. Note that if a stacked bar plot is generated, confidence intervals will not be plotted even if plot.ci = TRUE
           ci.u = upper_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
           ci.l = lower_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
           horiz = FALSE,                                   # desides if bars are vertically (FALSE) or horizontally (TRUE) oriented
           main = kmer,
           ylab = activities_and_deltas$ylabel,
           ##xlab = 'considered nucleotides\nrelative to poly(A) site',
           col=colors,
           cex.main=1.2,
           cex.sub=1.2,
           cex.lab=1.2,
           cex.names=1.2,
           cex.axis=1.2)
  ##abline(h=0)

  # create a legend  
  plot.new()
  par(xpd=TRUE)
  legend(x='topleft', 
         legend=rownames(activities_and_deltas$activities),
         cex=1.0, 
         col=colors,
         ##lwd=c(1.0,1.0),
         lty=0,
         pch=15,
         bg=par("bg"),
         ##inset=legend_inset,
         xpd=T)
  par(xpd=FALSE)
  
  dev.off();
  
  if(dim(activities_and_deltas$activities)[1] > 20){
    # create another plot with randomly selected samples
    # from the original dataset
    
    # select a random set of 10 samples
    if(length(selected_rows) == 0){
      selected_rows <- sort(sample(1:dim(activities_and_deltas$activities)[1],10, replace = F))
    }
    
    # ---------------------------------------------------------------------------
    # create the plot
    
    sampled_activities <- activities_and_deltas$activities[selected_rows,]
    sampled_deltas <- activities_and_deltas$deltas[selected_rows,]
    sampled_zscore <- activities_and_deltas$zscore[selected_rows,]
    # sampled_fov <- activities_and_deltas$fov[selected_rows,]
    # calculate the upper and the lower bounds
    upper_bound = sampled_activities + 1.96*sampled_deltas
    lower_bound = sampled_activities - 1.96*sampled_deltas
    
    # for z-scores
    zscores.upper_bound = sampled_zscore
    zscores.lower_bound = sampled_zscore
    
    # # for fov
    # fov.upper_bound = sampled_fov
    # fov.lower_bound = sampled_fov
    
    # create the layout
    # create the pdf
    pdf(paste(results_dir, '/', file_name, '_randSelection.pdf', sep=''), 
        height=14, width=21, useDingbats=F)
    # create the layout
    layout(matrix(c(1,1,2,3,3,4,5,5,6), nrow=3, ncol=3, byrow = TRUE))
    # get some nice colors (maximal 12 can be chosen)
    colors = brewer.pal(12, 'Paired')[1:nrow(sampled_activities)]
    
    # create the plot
    par(mar=c(10,8,2,2), mgp=c(4,1,0), las=2)
    barplot2(sampled_activities,
             names.arg = activities_and_deltas$regions[colnames(activities_and_deltas$activities),'name'],
             beside = TRUE,
             plot.ci = TRUE,                                  # logical. If TRUE, confidence intervals are plotted over the bars. Note that if a stacked bar plot is generated, confidence intervals will not be plotted even if plot.ci = TRUE
             ci.u = upper_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
             ci.l = lower_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
             horiz = FALSE,                                   # desides if bars are vertically (FALSE) or horizontally (TRUE) oriented
             main = kmer,
             ylab = activities_and_deltas$ylabel,
             ##xlab = 'considered nucleotides\nrelative to poly(A) site',
             col=colors,
             cex.main=1.2,
             cex.sub=1.2,
             cex.lab=1.2,
             cex.names=1.2,
             cex.axis=1.2)
    ##abline(h=0)
    
    # create a legend  
    plot.new()
    par(xpd=TRUE)
    legend(x='topleft', 
           legend=rownames(sampled_activities),
           cex=1.0, 
           col=colors,
           ##lwd=c(1.0,1.0),
           lty=0,
           pch=15,
           bg=par("bg"),
           ##inset=legend_inset,
           xpd=T)
    par(xpd=FALSE)
    
    # #plot.new()
    # par(mar=c(10,8,2,2), mgp=c(4,1,0), las=2)
    # barplot2(sampled_fov,
    #          names.arg = activities_and_deltas$regions[colnames(activities_and_deltas$activities),'name'],
    #          beside = TRUE,
    #          plot.ci = TRUE,                                  #logical. If TRUE, confidence intervals are plotted over the bars. Note that if a stacked bar plot is generated, confidence intervals will not be plotted even if plot.ci = TRUE
    #          ci.u = fov.upper_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
    #          ci.l = fov.lower_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
    #          horiz = FALSE,                                   # desides if bars are vertically (FALSE) or horizontally (TRUE) oriented
    #          main = "fov",
    #          ylab = activities_and_deltas$ylabel,
    #          ##xlab = 'considered nucleotides\nrelative to poly(A) site',
    #          col=colors,
    #          cex.main=1.2,
    #          cex.sub=1.2,
    #          cex.lab=1.2,
    #          cex.names=1.2,
    #          cex.axis=1.2)
    plot(0, type ="n", axes = F, xlab = "", ylab = "")
    
    # create a legend  
    plot.new()
    par(xpd=TRUE)
    legend(x='topleft', 
           legend=rownames(sampled_activities),
           cex=1.0, 
           col=colors,
           ##lwd=c(1.0,1.0),
           lty=0,
           pch=15,
           bg=par("bg"),
           ##inset=legend_inset,
           xpd=T)
    par(xpd=FALSE)
    
    # plot the z-scores
    par(mar=c(10,8,2,2), mgp=c(4,1,0), las=2)
    barplot2(sampled_zscore,
             names.arg = activities_and_deltas$regions[colnames(activities_and_deltas$activities),'name'],
             beside = TRUE,
             plot.ci = TRUE,                                  #logical. If TRUE, confidence intervals are plotted over the bars. Note that if a stacked bar plot is generated, confidence intervals will not be plotted even if plot.ci = TRUE
             ci.u = zscores.upper_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
             ci.l = zscores.lower_bound,                              # The confidence intervals (ci.l = lower bound, ci.u = upper bound) to be plotted if plot.ci = TRUE. Values must have the same dim structure as height.
             horiz = FALSE,                                   # desides if bars are vertically (FALSE) or horizontally (TRUE) oriented
             main = "z-scores",
             ylab = activities_and_deltas$ylabel,
             ##xlab = 'considered nucleotides\nrelative to poly(A) site',
             col=colors,
             cex.main=1.2,
             cex.sub=1.2,
             cex.lab=1.2,
             cex.names=1.2,
             cex.axis=1.2)
    
    # create a legend
    plot.new()
    par(xpd=TRUE)
    legend(x='topleft', 
           legend=rownames(sampled_activities),
           cex=1.0, 
           col=colors,
           ##lwd=c(1.0,1.0),
           lty=0,
           pch=15,
           bg=par("bg"),
           ##inset=legend_inset,
           xpd=T)
    par(xpd=FALSE)
    
    dev.off();
  }
}

# -----------------------------------------------------------------------------

# write out a file of the k-mers that we plotted
write_incl_rownames(data=kmers.to_plot,
                    col_name='idx', 
                    filename=file.path(results_dir, '..', "mean_diff_zscores.tsv", fsep=.Platform$file.sep))
