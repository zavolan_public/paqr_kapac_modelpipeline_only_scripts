#!/usr/bin/env python
"""
Count kmers in a given region. Output matrix in tsv format.
"""

__date__ = "2017-10-15"
__author__ = "Maciej Bak"
__email__ = "maciej.bak@unibas.ch"
__license__ = "GPL"

# imports
import sys
import time
import logging
from argparse import ArgumentParser, RawTextHelpFormatter
import itertools
import os


def parse_arguments():
    parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)
    parser.add_argument("-v",
                    "--verbosity",
                    dest="verbosity",
                    choices=('DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL'),
                    default='ERROR',
                    help="Verbosity/Log level. Defaults to ERROR")
    parser.add_argument("-l",
                    "--logfile",
                    dest="logfile",
                    help="Store log to this file.")
    parser.add_argument("--window-dir",
                    dest="window_dir",
                    required=True,
                    help="Path to the I/O directory. IN: sequences.fasta, OUT: matrix.tsv")
    parser.add_argument("--kmer-min",
                    dest="kmer_min",
                    required=True,
                    help="Minimal length of kmers.")
    parser.add_argument("--kmer-max",
                    dest="kmer_max",
                    required=True,
                    help="Maximal length of kmers.")
    return parser


def kmer_list_without_overlaps(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches


def kmer_list(dna, k):
    result = []
    for x in range(len(dna)+1-k): result.append(dna[x:x+k])
    return result


def main(options, logger):

    kmers={} #create a dict of all possible kmers
    for l in range(int(options.kmer_min),int(options.kmer_max)+1):
        for i in itertools.product(["A","C","G","T"], repeat=l): kmers["".join(i)]=0

    #read line by line of fasta file and count kmers
    with open(os.path.join(options.window_dir,"sequences.fasta"),'r') as in_file, open(os.path.join(options.window_dir,"matrix.tsv"),'w') as out_file:

        lines = in_file.read().splitlines()
        window = options.window_dir.split("/")[-1]

        #write headers
        out_file.write("pas")
        for kmer in sorted(kmers.keys()): out_file.write("\t"+kmer)
        out_file.write("\n")

        #fill dicts
        for line in range(0,len(lines),2):
            seq = lines[line+1]
            if seq.count("N") == 0: #omit regions with flanking regions containing N base
                site_coord = lines[line].split(">")[1]
                out_file.write(site_coord)
                #Here we count kmers:
                #without overlaps
                for kmer in kmers.keys(): kmers[kmer] = len(list(kmer_list_without_overlaps(seq,kmer)))
                #with overlaps
                #for l in range(int(options.kmer_min),int(options.kmer_max)+1): #over kmers of fixed length | len-k+1 of all possible k-mers
                #    c = collections.Counter(kmer_list(win,l))
                #    for kmer in c: kmers[kmer] = c[kmer]
                for kmer in sorted(kmers.keys()): out_file.write("\t"+str(kmers[kmer]))
                out_file.write("\n")


if __name__ == '__main__':
    try:
        try:
            options = parse_arguments().parse_args()
        except Exception as e:
            parser.print_help()
            sys.exit()
        #
        ########################################################################
        # Set up logging
        ########################################################################
        #
        formatter = logging.Formatter(fmt="[%(asctime)s] %(levelname)s - %(message)s",
                                      datefmt="%d-%b-%Y %H:%M:%S")
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logger = logging.getLogger('uniprot_to_json')
        logger.setLevel(logging.getLevelName(options.verbosity))
        logger.addHandler(console_handler)
        if options.logfile is not None:
            logfile_handler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=50000, backupCount=2)
            logfile_handler.setFormatter(formatter)
            logger.addHandler(logfile_handler)
        #
        ########################################################################
        #
        start_time = time.time()
        start_date = time.strftime("%d-%m-%Y at %H:%M:%S")
        #
        ########################################################################
        # Run main
        ########################################################################
        #
        logger.info("Starting script")
        main(options, logger)
        #
        ########################################################################
        seconds = time.time() - start_time
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        logger.info("Successfully finished in {hours} hour(s) {minutes} minute(s) and {seconds} second(s)".format(
            hours=int(hours),
            minutes=int(minutes),
            seconds=int(seconds) if seconds > 1.0 else 1
        ))
    except KeyboardInterrupt as e:
        logger.warn("Interrupted by user after {hours} hour(s) {minutes} minute(s) and {seconds} second(s)".format(
            hours=int(hours),
            minutes=int(minutes),
            seconds=int(seconds) if seconds > 1.0 else 1
        ))
        sys.exit(-1)
    except Exception as e:
        logger.exception(str(e))
        raise e

