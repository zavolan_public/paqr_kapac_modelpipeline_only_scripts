#!/usr/bin/env python
"""
Given a txtfile with window region coordinates (relative to a specific site) extract corresponding absolute coordinates
for provided sites set and the sequences of the regions.
"""

__date__ = "2017-10-14"
__author__ = "Maciej Bak"
__email__ = "maciej.bak@unibas.ch"
__license__ = "GPL"

# imports
import sys
import time
import logging
from argparse import ArgumentParser, RawTextHelpFormatter
import os
import pybedtools


def parse_arguments():
    parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)
    parser.add_argument("-v",
                    "--verbosity",
                    dest="verbosity",
                    choices=('DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL'),
                    default='ERROR',
                    help="Verbosity/Log level. Defaults to ERROR")
    parser.add_argument("-l",
                    "--logfile",
                    dest="logfile",
                    help="Store log to this file.")
    parser.add_argument("--bed",
                    dest="SUPPA_SE_bed",
                    required=True,
                    help="Bedfile with the coordinates of splicing site from SUPPA.")
    parser.add_argument("--genome",
                    dest="genome",
                    required=True,
                    help="Genome in fasta format.")
    parser.add_argument("--outdir",
                    dest="outdir",
                    required=True,
                    help="Directory for window folders.")
    return parser


def main(options, logger):

    window_dir = options.outdir
    coords = [i[1:].split("to") for i in window_dir.split("/")[-1].split(".")]
    coords = [coords[0][0],coords[0][1],coords[1][0],coords[1][1]]

    #Extract region coordinates into bed
    if (int(coords[0]) and not int(coords[3])): #whole window upstream interesting site
        assert not int(coords[2])
        beg = coords[0]
        end = coords[1]
        # mod by RS: plus strand only upstream reg: end pos: $3 instead of $2
        #                        only downstream reg: start pos: $2 instead of $3
        #           minus strand only upstream reg: start pos: $2 instead of $3
        #                        only downstream reg: end pos: $3 instead of $2 as end pos
        command = "cat "+options.SUPPA_SE_bed+" | awk -F \"\\t\" \
        '{{ if ($6 == \"+\") print $1\"\\t\"$2-"+beg+"\"\\t\"$3-"+end+"\"\\t\"$4\"\\t\"$5\"\\t\"$6; else if ($6 == \"-\") print $1\"\\t\"$2+"+end+"\"\\t\"$3+"+beg+"\"\\t\"$4\"\\t\"$5\"\\t\"$6}}' \
        1> "+os.path.join(window_dir,"coordinates.bed")

    elif (int(coords[3]) and not int(coords[0])): #whole window downstream interesting site
        assert not int(coords[1])
        beg = coords[2]
        end = coords[3]
        command = "cat "+options.SUPPA_SE_bed+" | awk -F \"\\t\" \
        '{{ if ($6 == \"+\") print $1\"\\t\"$2+"+beg+"\"\\t\"$3+"+end+"\"\\t\"$4\"\\t\"$5\"\\t\"$6; else if ($6 == \"-\") print $1\"\\t\"$2-"+end+"\"\\t\"$3-"+beg+"\"\\t\"$4\"\\t\"$5\"\\t\"$6}}' \
        1> "+os.path.join(window_dir,"coordinates.bed")

    else: #window goes through interesting site
        assert (not int(coords[1]) and not int(coords[2]))
        beg = coords[0]
        end = coords[3]
        command = "cat "+options.SUPPA_SE_bed+" | awk -F \"\\t\" \
        '{{ if ($6 == \"+\") print $1\"\\t\"$2-"+beg+"\"\\t\"$3+"+end+"\"\\t\"$4\"\\t\"$5\"\\t\"$6; else if ($6 == \"-\") print $1\"\\t\"$2-"+end+"\"\\t\"$3+"+beg+"\"\\t\"$4\"\\t\"$5\"\\t\"$6}}' \
        1> "+os.path.join(window_dir,"coordinates.bed")

    os.system(command)

    #Extract sequence from regions
    BedTool = pybedtools.BedTool(os.path.join(window_dir,"coordinates.bed"))
    # mod by RS: included "s=True" to force strandedness!
    BedTool = BedTool.sequence(fi=options.genome,s=True)
    with open(os.path.join(window_dir,"sequences.fasta"),"w") as outfile, open(options.SUPPA_SE_bed,"r") as ss_file:
        fasta_lines = open(BedTool.seqfn).read().splitlines()
        ss_lines = ss_file.read().splitlines()
        assert len(fasta_lines)/2 == len(ss_lines) #more: these files are sorted accordingly since #1 comes from #2
        for i in range(0,len(fasta_lines),2):
            ss_coordinate = ss_lines[int(i/2)].split("\t")[3]
            outfile.write(">"+ss_coordinate+"\n")
            outfile.write(fasta_lines[i+1]+"\n")


if __name__ == '__main__':
    try:
        try:
            options = parse_arguments().parse_args()
        except Exception as e:
            parser.print_help()
            sys.exit()
        #
        ########################################################################
        # Set up logging
        ########################################################################
        #
        formatter = logging.Formatter(fmt="[%(asctime)s] %(levelname)s - %(message)s",
                                      datefmt="%d-%b-%Y %H:%M:%S")
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logger = logging.getLogger('uniprot_to_json')
        logger.setLevel(logging.getLevelName(options.verbosity))
        logger.addHandler(console_handler)
        if options.logfile is not None:
            logfile_handler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=50000, backupCount=2)
            logfile_handler.setFormatter(formatter)
            logger.addHandler(logfile_handler)
        #
        ########################################################################
        #
        start_time = time.time()
        start_date = time.strftime("%d-%m-%Y at %H:%M:%S")
        #
        ########################################################################
        # Run main
        ########################################################################
        #
        logger.info("Starting script")
        main(options, logger)
        #
        ########################################################################
        seconds = time.time() - start_time
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        logger.info("Successfully finished in {hours} hour(s) {minutes} minute(s) and {seconds} second(s)".format(
            hours=int(hours),
            minutes=int(minutes),
            seconds=int(seconds) if seconds > 1.0 else 1
        ))
    except KeyboardInterrupt as e:
        logger.warn("Interrupted by user after {hours} hour(s) {minutes} minute(s) and {seconds} second(s)".format(
            hours=int(hours),
            minutes=int(minutes),
            seconds=int(seconds) if seconds > 1.0 else 1
        ))
        sys.exit(-1)
    except Exception as e:
        logger.exception(str(e))
        raise e

